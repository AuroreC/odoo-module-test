# -*- coding: utf-8 -*-
{
    'name': "School",

    'summary': "Module custom de test",

    'description': """
        Long description of module's purpose
    """,

    'author': "DATAKODE",
    'website': "https://www.datakode.fr/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Extra Tools',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'mail'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/student.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'installable': True,
    # Pour montrer le module dans Apps directement
    'application' : True,
    # Ne pas installer le module automatiquement
    'auto_install': False
}
